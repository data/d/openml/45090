# OpenML dataset: Leukemia

https://www.openml.org/d/45090

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Leukemia (Leukemia-2, AML, AML-2) dataset**

**Authors**: Golub, D. Slonim, P. Tamayo, C. Huard, M. Gaasenbeek, J. Mesirov, H. Coller, M. Loh, J. Downing, M. Caligiuri, et al

**Please cite**: ([URL](https://www.science.org/doi/full/10.1126/science.286.5439.531)): Golub, D. Slonim, P. Tamayo, C. Huard, M. Gaasenbeek, J. Mesirov, H. Coller, M. Loh, J. Downing, M. Caligiuri, et al, Molecular classification of cancer: class discovery and class prediction by gene expression monitoring, Science 286 (5439) (1999) 531-53

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45090) of an [OpenML dataset](https://www.openml.org/d/45090). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45090/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45090/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45090/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

